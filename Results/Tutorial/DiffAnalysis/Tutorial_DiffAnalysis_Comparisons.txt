Number of DEGs for each contrast:
                                                                      Contrast Nb_DEG
1                                                        [Genotype1-Genotype2]   6413
2                                                        [Genotype1-Genotype3]   6410
3                                                        [Genotype1-Genotype4]   3427
4                                                        [Genotype2-Genotype3]    774
5                                                        [Genotype2-Genotype4]   5727
6                                                        [Genotype3-Genotype4]   7137
7                                                            [control-treated]  13702
8                                        [control_Genotype1-control_Genotype2]   4385
9                                        [control_Genotype1-control_Genotype3]   3531
10                                       [control_Genotype1-control_Genotype4]   1739
11                                       [control_Genotype2-control_Genotype3]    546
12                                       [control_Genotype2-control_Genotype4]   2087
13                                       [control_Genotype3-control_Genotype4]   3181
14                                       [treated_Genotype1-treated_Genotype2]   4843
15                                       [treated_Genotype1-treated_Genotype3]   4837
16                                       [treated_Genotype1-treated_Genotype4]   3311
17                                       [treated_Genotype2-treated_Genotype3]    239
18                                       [treated_Genotype2-treated_Genotype4]   4409
19                                       [treated_Genotype3-treated_Genotype4]   5728
20                                       [Genotype1_control-Genotype1_treated]   9979
21                                       [Genotype2_control-Genotype2_treated]   8723
22                                       [Genotype3_control-Genotype3_treated]   8063
23                                       [Genotype4_control-Genotype4_treated]   9614
24 [Genotype1_control-Genotype1_treated]-[Genotype2_control-Genotype2_treated]   2547
25 [Genotype1_control-Genotype1_treated]-[Genotype3_control-Genotype3_treated]   1991
26 [Genotype1_control-Genotype1_treated]-[Genotype4_control-Genotype4_treated]   2488
27 [Genotype2_control-Genotype2_treated]-[Genotype3_control-Genotype3_treated]     72
28 [Genotype2_control-Genotype2_treated]-[Genotype4_control-Genotype4_treated]    936
29 [Genotype3_control-Genotype3_treated]-[Genotype4_control-Genotype4_treated]    822

                                                                      Contrast Expression Nb_DEG
1                                                        [Genotype1-Genotype2]         Up   3110
2                                                        [Genotype1-Genotype2]       Down   3303
3                                                        [Genotype1-Genotype3]         Up   3122
4                                                        [Genotype1-Genotype3]       Down   3288
5                                                        [Genotype1-Genotype4]         Up   2070
6                                                        [Genotype1-Genotype4]       Down   1357
7                                                        [Genotype2-Genotype3]         Up    345
8                                                        [Genotype2-Genotype3]       Down    429
9                                                        [Genotype2-Genotype4]         Up   3082
10                                                       [Genotype2-Genotype4]       Down   2645
11                                                       [Genotype3-Genotype4]         Up   3810
12                                                       [Genotype3-Genotype4]       Down   3327
13                                                           [control-treated]         Up   6773
14                                                           [control-treated]       Down   6929
15                                       [control_Genotype1-control_Genotype2]         Up   2122
16                                       [control_Genotype1-control_Genotype2]       Down   2263
17                                       [control_Genotype1-control_Genotype3]         Up   1623
18                                       [control_Genotype1-control_Genotype3]       Down   1908
19                                       [control_Genotype1-control_Genotype4]         Up    859
20                                       [control_Genotype1-control_Genotype4]       Down    880
21                                       [control_Genotype2-control_Genotype3]         Up    284
22                                       [control_Genotype2-control_Genotype3]       Down    262
23                                       [control_Genotype2-control_Genotype4]         Up   1017
24                                       [control_Genotype2-control_Genotype4]       Down   1070
25                                       [control_Genotype3-control_Genotype4]         Up   1724
26                                       [control_Genotype3-control_Genotype4]       Down   1457
27                                       [treated_Genotype1-treated_Genotype2]         Up   2372
28                                       [treated_Genotype1-treated_Genotype2]       Down   2471
29                                       [treated_Genotype1-treated_Genotype3]         Up   2429
30                                       [treated_Genotype1-treated_Genotype3]       Down   2408
31                                       [treated_Genotype1-treated_Genotype4]         Up   2225
32                                       [treated_Genotype1-treated_Genotype4]       Down   1086
33                                       [treated_Genotype2-treated_Genotype3]         Up     97
34                                       [treated_Genotype2-treated_Genotype3]       Down    142
35                                       [treated_Genotype2-treated_Genotype4]         Up   2542
36                                       [treated_Genotype2-treated_Genotype4]       Down   1867
37                                       [treated_Genotype3-treated_Genotype4]         Up   3092
38                                       [treated_Genotype3-treated_Genotype4]       Down   2636
39                                       [Genotype1_control-Genotype1_treated]         Up   4772
40                                       [Genotype1_control-Genotype1_treated]       Down   5207
41                                       [Genotype2_control-Genotype2_treated]         Up   4212
42                                       [Genotype2_control-Genotype2_treated]       Down   4511
43                                       [Genotype3_control-Genotype3_treated]         Up   3905
44                                       [Genotype3_control-Genotype3_treated]       Down   4158
45                                       [Genotype4_control-Genotype4_treated]         Up   4822
46                                       [Genotype4_control-Genotype4_treated]       Down   4792
47 [Genotype1_control-Genotype1_treated]-[Genotype2_control-Genotype2_treated]         Up   1436
48 [Genotype1_control-Genotype1_treated]-[Genotype2_control-Genotype2_treated]       Down   1111
49 [Genotype1_control-Genotype1_treated]-[Genotype3_control-Genotype3_treated]         Up   1119
50 [Genotype1_control-Genotype1_treated]-[Genotype3_control-Genotype3_treated]       Down    872
51 [Genotype1_control-Genotype1_treated]-[Genotype4_control-Genotype4_treated]         Up   1717
52 [Genotype1_control-Genotype1_treated]-[Genotype4_control-Genotype4_treated]       Down    771
53 [Genotype2_control-Genotype2_treated]-[Genotype3_control-Genotype3_treated]         Up     29
54 [Genotype2_control-Genotype2_treated]-[Genotype3_control-Genotype3_treated]       Down     43
55 [Genotype2_control-Genotype2_treated]-[Genotype4_control-Genotype4_treated]         Up    668
56 [Genotype2_control-Genotype2_treated]-[Genotype4_control-Genotype4_treated]       Down    268
57 [Genotype3_control-Genotype3_treated]-[Genotype4_control-Genotype4_treated]         Up    622
58 [Genotype3_control-Genotype3_treated]-[Genotype4_control-Genotype4_treated]       Down    200
